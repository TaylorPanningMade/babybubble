from django.db import models
from django.utils import timezone


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


def user_directory_path(instance, filename):
    filename = instance.name
    return "{0}/{1}".format(instance.owner.id, filename)


class User(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    description = models.TextField(max_length=1000)
    created_on = models.DateField(default=timezone.now)
    updated_at = AutoDateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.name}"


class Item(models.Model):
    GENDER_CHOICES = (
        ("M", "Male"),
        ("F", "Female"),
        ("U", "Unisex"),
    )
    SIZE_CHOICES = (
        ("Toy", "2-9 Pounds"),
        ("Small", "10-34 Pounds"),
        ("Medium", "35-54 Pounds"),
        ("Large", "55-74 Pounds"),
        ("Giant", "75-120+ Pounds"),
    )
    make = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    age = models.SmallIntegerField(null=True, blank=True)
    image = models.FileField(
        upload_to=user_directory_path, blank=True, null=True)
    description = models.TextField(max_length=1000, null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    size = models.CharField(max_length=6, choices=SIZE_CHOICES)
    added_on = models.DateField(default=timezone.now)
    checked_out_on = AutoDateTimeField(
        default=timezone.now, null=True, blank=True)
    owner = models.ForeignKey(
        User, related_name="user", on_delete=models.CASCADE, null=False
    )

    def __str__(self):
        return f"{self.Make} {self.Model} Owned by: {self.owner}"


class Tag(models.Model):
    name = models.CharField(max_length=45)
    items = models.ForeignKey(
        Item, related_name="Tag", on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.name}, All items with this tag:{self.items}"
