from django.contrib import admin
from django.urls import path
from .views import list_all_items, list_all_users

urlpatterns = [
    path("list_items/", list_all_items, name="list_all_items"),
    path("list_users/", list_all_users, name="list_all_users"),
]
