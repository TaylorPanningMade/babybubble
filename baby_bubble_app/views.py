from django.shortcuts import render

from .models import Item, User, Tag
from .encoders import ItemEncoder, UserEncoder, TagEncoder
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

# Create your views here.


@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_all_items(request):
    if request.method == "GET":
        items = Item.objects.all()
        return JsonResponse(
            {"items": items},
            encoder=ItemEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            user_id = content["owner"]
            owner = User.objects.get(id=user_id)
            content["owner"] = owner
            item = Item.objects.create(**content)
            return JsonResponse(
                item,
                encoder=ItemEncoder,
                safe=False,
            )
        except Item.DoesNotExist:
            response = JsonResponse({"message": "Could not create the Item"})
            response.status_code = 400
            return response


@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_all_users(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse(
            {"users": users},
            encoder=UserEncoder,
        )
    # only implementing the get users function at the moment
    # will need to do the add user differently. Check the info from Galv.

    # else:
    #     try:
    #         content = json.loads(request.body)
    #         user_id = content["owner"]
    #         owner = User.objects.get(id=user_id)
    #         content["owner"] = owner
    #         item = Item.objects.create(**content)
    #         return JsonResponse(
    #             item,
    #             encoder=ItemEncoder,
    #             safe=False,
    #         )
    #     except Item.DoesNotExist:
    #         response = JsonResponse({"message": "Could not create the Item"})
    #         response.status_code = 400
    #         return response
