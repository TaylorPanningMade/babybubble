from django.apps import AppConfig


class BabyBubbleAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'baby_bubble_app'
