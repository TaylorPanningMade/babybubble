

from .models import Item, Tag, Account
from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from django.db import models


class ImageEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, models.FileField):
            return o.name
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(ImageEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class AccountListEncoder(ModelEncoder):
    model = Account
    properties = ["first_name", "id", "username"]


class AccountDetailEncoder(ModelEncoder):
    model = Account
    properties = [
        "id",
        "email",
        "first_name",
        "last_name",
        "username",
        "password",
        "is_active",
        #"date_joined",
        "censored",
    ]


class ItemEncoder(ModelEncoder):
    model = Item
    properties = [
        "id",
        "make",
        "model",
        "gender",
        "size",
        "condition",
        "owner",
        "description",
        "available",
        # "added_on",
        # "checked_out_on",
    ]
    encoders = {"Account": AccountDetailEncoder()}

    def get_extra_data(self, o):
        return {"image": o.id}


class TagEncoder(ModelEncoder):
    model = Tag
    properties = [
        'id',
        'name',
        'items',
    ]

    encoders = {'items': ItemEncoder()}
