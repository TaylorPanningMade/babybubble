from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


def user_directory_path(instance, filename):
    filename = instance.model
    return "{0}/{1}".format(instance.make, filename)


class Account(AbstractUser):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    censored = models.BooleanField(default=True, null=True)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    last_login = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return f"{self.first_name, self.last_name, self.username}"


class Item(models.Model):
    GENDER_CHOICES = (
        ("M", "Male"),
        ("F", "Female"),
        ("U", "Unisex"),
    )
    SIZE_CHOICES = (
        ("Toy", "2-9 Pounds"),
        ("Small", "10-34 Pounds"),
        ("Medium", "35-54 Pounds"),
        ("Large", "55-74 Pounds"),
        ("Giant", "75-120+ Pounds"),
    )
    CONDITION_CHOICES = (
        ("N", "New"),
        ("LN", "Like New"),
        ("U", "Used"),
        ("R", "Rat-a-tat")
    )

    make = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    condition = models.CharField(max_length=10, choices=CONDITION_CHOICES)
    image = models.FileField(
        upload_to=user_directory_path, blank=True, null=True)

    description = models.TextField(max_length=1000, null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    size = models.CharField(max_length=6, choices=SIZE_CHOICES)
    added_on = models.DateField(default=timezone.now)
    checked_out_on = AutoDateTimeField(
        default=timezone.now, null=True, blank=True)

    available = models.BooleanField(default=True)
    owner = models.ForeignKey(
        Account, related_name="Account", on_delete=models.CASCADE, null=False
    )

    def __str__(self):
        return f"{self.make} {self.model} Owned by: {self.owner}"


class Tag(models.Model):
    name = models.CharField(max_length=45)
    items = models.ManyToManyField("Item", related_name="tag")

    def __str__(self):
        return f"{self.name}"
