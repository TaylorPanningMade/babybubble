from django.contrib import admin
from django.urls import path
from .views import (
    list_all_items,
    show_delete_update_item,
    api_show_account,
    api_list_accounts,
    update_censors,
    register,
    login_google,
    auth_options,
    webauthn_registration_options,
    webauthn_registration_verification,
)

urlpatterns = [
    path("list_items/", list_all_items, name="list_all_items"),
    path('list_items/<int:pk>', show_delete_update_item, name="i_item"),
    path("censor/<int:pk>", update_censors, name="censors"),
    path("accounts/list", api_list_accounts, name="accounts_list"),
    path("accounts/<int:pk>", api_show_account, name="account_detail"),
    path("accounts/", register, name="register_account"),
    path("accounts/google/", login_google, name="federated_google"),
    path("accounts/options/", auth_options, name="account_login_options"),
    path("accounts/options/registration/webauthn/",
         webauthn_registration_options, name="webauthn_registration_options"),
    path("accounts/options/registration/webauthn/verification/",
         webauthn_registration_verification, name="webauthn_registration_verification"),
]
