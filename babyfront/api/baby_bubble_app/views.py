import djwto.authentication as auth
import jwt
from bcrypt import checkpw
from django.contrib.auth import authenticate, logout
from django.contrib.auth.hashers import make_password
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic.edit import CreateView
from rest_framework_jwt.utils import jwt_decode_handler
from webauthn import PublicKeyCredentialCreationOptions, PublicKeyCredentialRpEntity
from webauthn.exceptions import (
    InvalidAuthenticationError,
    InvalidChallengeError,
    InvalidOriginError,
    InvalidRegistrationError,
)
from webauthn.helpers import generate_challenge, verify_origin
from webauthn.server import SimpleWebAuthnServer

from .encoders import AccountDetailEncoder, AccountListEncoder, ItemEncoder
from .models import Account, Item  # , Tag

from secrets import token_bytes
from base64 import b64encode


def encode():
    encryptedPassword = b64encode(token_bytes(32))
    return encryptedPassword


def remove_encryption(code):
    readable_code = code.decode()
    return readable_code


@require_http_methods(["GET"])
def list_all_items(request):
    if request.method == "GET":
        items = Item.objects.all()
        return JsonResponse(
            {"items": items},
            encoder=ItemEncoder,
        )
        # Will implement a new method once we add a new one
    # else:
    #     try:
    #         content = json.loads(request.body)
    #         print('!!!!!!!!!!content!!!!!!!!!!!', content)
    #         content['owner'] = token_data["user"]
    #         content['added_on'] = date.today().isoformat()
    #         content['checked_out_on'] = ''
    #         content['available'] = True
    #         content.pop('submitted')
    #         item = Item.objects.create(**content)
    #         return JsonResponse(
    #             item,
    #             encoder=ItemEncoder,
    #             safe=False,
    #         )
    #     except Item.DoesNotExist:
    #         response = JsonResponse({"message": "Could not create the Item"})
    #         response.status_code = 400
    #         return response


# ! * ! * ! * ! * ! * ! * ! * ! * ! * !
# everything below this line will be the new login information
# ! * ! * ! * ! * ! * ! * ! * ! * ! * !

# HELPER FUNCTIONS
rp_id = "localhost:3000"
expected_origin = "https://localhost:3000"


def find_user(search_email):
    print(search_email)
    account = Account.objects.filter(email=search_email)
    if account:
        return account
    else:
        return "User credentials invalid (you need to register)"


def credential_to_dict(credential):
    return {
        "type": credential["type"],
        "id": credential["id"],
        "rawId": credential["rawId"],
        "response": {
            "clientDataJSON": credential["response"]["clientDataJSON"],
            "authenticatorData": credential["response"]["authenticatorData"],
            "signature": credential["response"]["signature"],
            "userHandle": credential["response"].get("userHandle", ""),
        },
    }


# NORMAL THINGS
def login(request):
    email = request.POST.get("email")
    password = request.POST.get("password")

    user_found = find_user(email)
    if user_found and bcrypt.checkpw(
        password.encode("utf-8"), user_found[0].password.encode("utf-8")
    ):
        return JsonResponse(
            {"ok": True, "name": user_found.name, "email": user_found.email}
        )
    else:
        return JsonResponse({"ok": False, "message": "Credentials are wrong"})


# Registration of users
@csrf_exempt
@require_http_methods(["GET", "POST"])
def register(request):
    name = request.POST.get("name")
    email = request.POST.get("email")
    password = request.POST.get("password")
    user = find_user(email)
    if user:
        response_data = {"ok": False, "message": "User already exists"}
    else:
        hashed_password = make_password(password)
        Account.objects.create(name=name, email=email, password=hashed_password)
        response_data = {"ok": True}
    return JsonResponse(response_data)


# FEDERATED LOGIN WITH GOOGLE


def login_google(request):
    jwt_token = request.POST.get("credential", {}).get("credential", None)
    if jwt_token:
        try:
            decoded = jwt.decode(jwt_token, verify=False)
            email = decoded.get("email", None)
            given_name = decoded.get("given_name", "")
            family_name = decoded.get("family_name", "")
            name = f"{given_name} {family_name}".strip()

            user = {
                "email": email,
                "name": name,
                "password": None,
                "federated": {"google": decoded.get("aud", None)},
            }

            user_found = find_user(email)
            if user_found:
                user_found["federated"] = user["federated"]
                Account.objects.update_or_create(user_found)
            else:
                Account.objects.update_or_create(user_found)
            return JsonResponse({"ok": True, "name": name, "email": email})
        except jwt.DecodeError:
            pass

    return JsonResponse({"ok": False})


# WEB AUTH
# define the options that a user has to login
@require_http_methods(["GET", "POST"])
def auth_options(request):
    print(request)
    email = request.POST.get("email")
    user = find_user(email)
    if user:
        response_data = {
            "password": bool(user.password),
            "google": user.federated and user.federated.get("google", False),
            "webauthn": user.webauthn,
        }
    else:
        response_data = {"password": True}
    return JsonResponse(response_data)


# WEB AUTHN REGISTRATION OPTIONS
@csrf_exempt
@require_http_methods(["GET", "POST"])
def webauthn_registration_options(request):
    email = request.POST.get("email")
    user = find_user(email)

    rp_entity = WebAuthnPublicKeyCredentialRpEntity("Baby Bubble")
    user_entity = WebAuthnPublicKeyCredentialUserEntity(
        user.id, user.email, user.name, b""
    )
    options = PublicKeyCredentialCreationOptions(
        PublicKeyCredentialRpEntity(rp_entity, rp_id),
        PublicKeyCredentialUserEntity(user_entity),
        challenge=b"random_challenge_here",
        pub_key_cred_params=[
            {"type": "public-key", "alg": -7},
            {"type": "public-key", "alg": -257},
        ],
        timeout=60000,
        exclude_credentials=[],
        authenticator_selection={
            "user_verification": "required",
            "resident_key": "required",
        },
        attestation="none",
    )
    request.session["creation_options"] = options
    request.session["email"] = email
    return JsonResponse(options.registration_dict)


# Registration view
@csrf_exempt
@require_http_methods(["GET", "POST"])
def webauthn_registration_verification(request):
    if request.method != "POST":
        return JsonResponse({"error": "Method not allowed"}, status=405)

    user = find_user(request.POST.get("email", ""))
    if not user:
        return JsonResponse({"error": "User not found"}, status=404)

    challenge = generate_challenge(32)
    user.current_challenge = challenge
    options = {
        "rp": {"id": rp_id, "name": "Example RP"},
        "user": {"id": user.id, "name": user.name, "displayName": user.display_name},
        "challenge": challenge,
        "pubKeyCredParams": [{"type": "public-key", "alg": -7}],
        "timeout": 60000,
        "attestation": "direct",
        "excludeCredentials": [],
        "authenticatorSelection": {"userVerification": "preferred"},
        "attestationConveyancePreference": "none",
    }

    # Generate registration options and return to client
    registration_options = SimpleWebAuthnServer.generate_registration_options(options)
    return JsonResponse(registration_options)
