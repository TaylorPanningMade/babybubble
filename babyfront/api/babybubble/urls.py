from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("djwto.urls")),
    path('api/', include('baby_bubble_app.urls')),
]
