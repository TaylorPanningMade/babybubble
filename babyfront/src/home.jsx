import React from 'react';

class Home extends React.Component {
    render() {
        return (

            <div className="App">
                <div
                    className="container-fluid d-flex align-items-center"
                    style={{
                        height: "100vh",
                        width: '100vw',
                        backgroundImage:
                            "url(https://images.pexels.com/photos/1276518/pexels-photo-1276518.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        opacity: 1.0,
                    }}
                >
                </div>
                </div>

        );
    }
}

export default Home

