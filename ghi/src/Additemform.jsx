import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useToken } from './accounts/token';
import { useNavigate } from 'react-router-dom';
// import AwsUpload from './components/Awsupload';

function AddItemForm(props) {
    const [account, setAccount] = useState([])
    // eslint-disable-next-line
    const [token, login, logout, signUp, update] = useToken();
    const [make, setMake] = useState('');
    const [model, setModel] = useState('');
    const [condition, setCondition] = useState('');
    const [image, setImage] = useState('');
    const [description, setDescription] = useState('');
    const [gender, setGender] = useState('');
    const [added_on, setAddedOn] = useState('');
    const [checked_out_on, setCheckedOutOn] = useState('');
    const [available, setAvailable] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [auth, setAuth] = useState([]);
    const JWT = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjozfQ.8hFkD3kuEK4EK4sR7_8YkyzZQR0RcJ2lcQsC1MlTJxo';
    

        // I dont think i need this
        // Not sure why I would need a delete function for the token
        //
        //
        //

    // useEffect(() => {
    //     async function getToken() {
    //         const userTokenUrl = `http://localhost:8000/api/get/token/`
    //         const request = await fetch(userTokenUrl, {
    //             method: "delete",
    //             credentials: "include",
    //             mode: "cors",
    //         })

    //         if (request.ok) {
    //             const responseData = await request.json()
    //             setAccount(responseData.token.username)
    //         }
    //     }
    //     getToken()
    // }, [])

    useEffect(() => {
        async function authen() {
            if (token !== null) {
                const tokenUrl = `http://localhost:8000/api/tokens/mine`;
                const request = await fetch(tokenUrl, {
                    method: "get",
                    credentials: "include",
                    mode: "cors",
                })
                if (request.ok) {
                    const toDa = await request.json()
                    if (toDa['token'] === token) {
                        setAuth(true)
                    }
                    else {
                        setAuth(false)
                    }
                }
                else {
                    setAuth(false)
                }
            }
            else {
                setAuth(false)
            }
        } authen();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []
    )

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            'make': make,
            'model': model,
            'condition': condition,
            'image': image,
            'description': description,
            'gender': gender,
            'added_on': added_on,
            'checked_out_on': checked_out_on,
            'available': available,
            'submitted': submitted,
        };

    //const ownersHost = `${process.env.REACT_APP_ACCOUNT_API}`
    const usersHost = 'http://localhost:8000/'
    const url = usersHost + `api/list_items/`;
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        let successTag = document.getElementById('success-message');
        let formTag = document.getElementById('create-item-form');
        successTag.classList.remove('d-none');
        formTag.classList.add('d-none');
        setAccount(token.id)
        setMake('')
        setModel('')
        setCondition('')
        setImage('')
        setDescription('')
        setGender('')
        setAddedOn('')
        setCheckedOutOn('')
        setAvailable('')
        setAuth(true)
        setSubmitted(true)
    }
    }
        return (
            <div className="App">
                <div
                    className="container-fluid d-flex align-items-center"
                    style={{
                        height: "100vh",
                        backgroundImage:
                            "url(https://images.pexels.com/photos/3845458/pexels-photo-3845458.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        opacity: .9,
                    }}
                >
                    <div className="signup" id="signuptop">
                        <div className="card mx-auto" style={{ width: "18rem" }}>
                            <div className="card-body">
                                <h1>Add a new item</h1>
                                <h3>  to your babybubble</h3>
                                <hr />
                                <form className='create-item-form' onSubmit={handleSubmit} id="create-item-form">
                                    <div className="form-floating mb-3">
                                        <input onChange={(e) => setMake(e.target.value)} value={make}
                                            placeholder="Make" required type="text" name="make"
                                            id="make" />
                                    </div>
                                    <div className="form-floating mb-3">
                                        <input onChange={(e) => setModel(e.target.value)} value={model}
                                            placeholder="Model" required type="text" name="model"
                                            id="model" />
                                    </div>
                                    <div className="form-floating mb-3">
                                        <select onChange={(e) => setCondition(e.target.value)} value={condition} required name="condition" id="condition" className="form-select">
                                            <option value="N">New</option>
                                            <option value="LN">Like New</option>
                                            <option value="U">Used</option>
                                            <option value="R">Rat-a-tat</option>
                                        </select>
                                    </div>
                                    <div className="form-floating mb-3">
                                        <textarea onChange={(e) => setDescription(e.target.value)} value={description}
                                            placeholder="Description" required type="text" name="description"
                                            id="description" />
                                    </div>
                                    {/* <div className="mb-3">
                                        <select onChange={e => setAccount} value={account} required name="user" 
                                                id="user" className="form-select">
                                            <option value="1">Select the owner of this item</option>
                                            <option value="2">someone else</option>
                                        </select>
                                    </div> */}
                                    <div className="form-floating mb-3">
                                        <select onChange={(e) => setGender(e.target.value)} value={gender} required name="gender" id="gender" className="form-select">
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                            <option value="U">Unisex</option>
                                        </select>
                                    </div>
                                    {/* <AwsUpload owner={account} model={model} className="form-floating mb-3" onChange={e => setImage} value={image} name="image" id="image"></AwsUpload> */}
                                    <button type="button" className="btn btn-success" onClick={handleSubmit}>submit</button>
                                </form>
                                <div className='success-message d-none' id="success-message">
                                    Congratulations!
                                    <br></br>
                                    You have added your item {make}, {model} to your village bubble
                                    <br></br>
                                    click here to add another item:
                                    <br></br>
                                    <Link to="/additem/" className="itemlink">
                                        <button type="button" className="btn btn-success">New Item</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }



export default AddItemForm;
