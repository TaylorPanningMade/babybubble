// import './App.css';
// import React from 'react';
// import 'bootstrap/dist/css/bootstrap.css';
// import { AuthProvider } from './accounts/auth';
// import Additemform from '../additemForm';
// import Home from '../home';
// import { connect } from 'react-redux';
// import * as actions from './store/actions/auth'

// function App() {
//   <div className="App">
//   </div>

//   return (
//     <AuthProvider>
//       <Home {...this.props}/>
//       <Additemform {...this.props}/>
//     </AuthProvider>
//   );
// }

// const mapStateToProps = state => {
//   return {
//     isAuthenticated: state.token !== null
//   }
// }

// const mapDispatchToProps = dispatch => {
//   return {
//     onTryAutoSignup: () => dispatch(actions.authCheckState())
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(App);
import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'

import Container from '@material-ui/core/Container'
import SignIn from './SignIn/SignIn'
import Register from './Register/Register'
import Home from './Home/Home'
import Public from './PublicPage/Public'
import NotFound from './NotFound/NotFound'
import Header from '../Components/Header'

export default function App() {

  const isLoggedIn = useSelector(state => state.user.isLoggedIn && state.user.jwt !== null)

  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      isLoggedIn === true
        ? <Component {...props} />
        : <Redirect to='/signin' />
    )} />
  )

  return (
    <Container maxWidth="lg">
      <Header isLoggedIn={isLoggedIn} />
      <Switch>
        <Route path='/' component={Public} exact />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={Register} />
        <PrivateRoute path='/Home' component={Home} />
        <Route component={NotFound} />
      </Switch>
    </Container>
  )
}
