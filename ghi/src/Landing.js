import React from "react"
import Container from "react-bootstrap/esm/Container"
import { Link } from "react-router-dom";
import Auth from "./accounts/Auth.js";

export function Landing(props) {

    function handleSubmit(event) {
        event.preventDefault();
        Auth.login(event);
    }

    function handleClick(event) {
        event.preventDefault();
        Auth.webAuthnLogin();
    }

    function handleRegister(event) {
        event.preventDefault();
        Auth.register(event);
    }

    function handleAddWebAuthn(event) {
        event.preventDefault();
        Auth.addWebAuthn();
    }

    function handleLogout(event) {
        event.preventDefault();
        Auth.Logout();
    }
        return (
            <Container>
                <section className="page" id="home">
                    <header>
                        <h2>Welcome!</h2>
                    </header>

                    <section className="logged_out">
                        <p>You are currently logged out.</p>
                        <p><a href="/login" class="navlink">Log in</a>.</p>
                    </section>

                    <section className="logged_in">
                        <p>You are currently logged in as <span class='account_name' className="navlink"></span>.</p>
                        <p>You can see details of your <a href='/account' className="navlink">Account</a></p>
                    </section>       
                </section>
                <section className="page"  id="register">
                    <h2>Register</h2>
                    <form id="formRegister" onsubmit={handleRegister}>
                        <fieldset>
                            <label for="register_name">Your Name</label>
                            <input id="register_name"
                                    required autocomplete="name"/>
                            <label for="register_email">Your Email</label>
                            <input id="register_email" 
                                    required type="email" autocomplete="username"/>
                            
                            <label for="register_password">Your Password</label>
                            <input type="password" id="register_password"
                                    required autocomplete="new-password"/>
                        </fieldset>

                        <button>Register Account</button>
                    </form>
                </section>

                <section class="page" id="login">
                    <h2>Log In</h2>

                    <form id="formLogin" onsubmit={handleSubmit}>
                        <fieldset>
                            <label for='login_email'>Your email</label>
                            <input id="login_email"
                                required autocomplete="username"/>      
                            <section hidden id="login_section_password">
                                <label for="login_password">Password</label>
                                <input type="password" id="login_password" autocomplete="current-password"
                                />
                            </section>
                            <section hidden id="login_section_webauthn">
                                <Link href="#" className="navLink"
                                    onclick={handleClick}>
                                    Log In with WebAuthn / Passkey </Link>
                            </section>
                        </fieldset>
                        <button onclick={Auth.checkAuthOptions()}>Continue</button>

                        <p>
                            <a href="/register" className="navlink">Register a new account instead</a> 
                        </p>    
             
                        
                        <fieldset> 
                        <div id="g_id_onload"
                            data-client_id="536693610759-0978e5ftivelvivdtps06j5hcgupmb9a"
                            data-context="signin"
                            data-ux_mode="popup"
                            data-callback="loginFromGoogle"
                            data-auto_prompt="false">
                        </div>

                        <div class="g_id_signin"
                            data-type="standard"
                            data-shape="pill"
                            data-theme="filled_black"
                            data-text="signin_with"
                            data-size="large"
                            data-logo_alignment="left">
                        </div>
                        </fieldset>
                    </form>            
                </section>

                
                <section className="page"  id="account">
                    <h2>My Account</h2>
                    <dl>
                        <dt>Name</dt>
                        <dd className="account_name"></dd>
                        <dt>Email</dt>
                        <dd className="account_username"></dd>
                    </dl>
                    <button onclick={handleAddWebAuthn}>Add Authenticator / Passkey</button>

                    <button onclick={handleLogout}>Log out</button>
                </section>
            </Container>
        )

}
export default Landing