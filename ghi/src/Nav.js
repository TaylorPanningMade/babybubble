import { NavLink } from 'react-router-dom';
import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import LogoutButton from './accounts/LogoutButton';
// import NavDropdown from 'react-bootstrap/NavDropdown';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const footerStyle = {
    backgroundColor: "",
    fontSize: "20px",
    color: "white",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%",
};

const phantomStyle = {
    display: "block",
    padding: "20px",
    height: "60px",
    width: "100%",
};

function NavbarDarkExample(props) {
    return (
        <Navbar className="navbar navbar-expand-lg ">
            <Container fluid>
                <Navbar.Brand href="#home">Baby Bubble</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbar-dark-example" />
                <Navbar.Collapse >
                    <Nav>
                        <ul className="navbar-nav me-auto mb-2">
                            <li className="nav-item">
                                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/additem/">Add Item</NavLink>
                            </li>
                            {/* <li className="nav-item">
                                <NavLink className="nav-link" to="/signup/">Signup</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/login/">Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/logout/">Logout</NavLink>
                            </li> */}
                        </ul>
                        <ul className="navbar-nav me-auto mb-2 bg-light">
                            <li className="nav-item"> 
                                <LogoutButton></LogoutButton>
                            </li>
                        </ul>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}


export default NavbarDarkExample;