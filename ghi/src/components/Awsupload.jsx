// import React from 'react'
// import { useState } from 'react';
// import { Link } from 'react-router-dom';
// import AWS from 'aws-sdk';

// class AwsUpload extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             photo: ''
//         };
//     }
//     UploadImageToS3 = (props) => {
//         const BUCKET_NAME = process.env.REACT_APP_AWS_STORAGE_BUCKET_NAME;
//         const AWSREGION = process.env.REACT_APP_REGION;
//         const KEY_ID = process.env.REACT_APP_AWS_ACCESS_KEY_ID;
//         const ACCESS_KEY = process.env.REACT_APP_AWS_SECRET_ACCESS_KEY;
//         const S3_BUCKET = BUCKET_NAME;
//         const REGION = AWSREGION;
//         AWS.config.update({
//             accessKeyId: KEY_ID,
//             secretAccessKey: ACCESS_KEY
//         })
//         const myBucket = new AWS.S3({
//             params: { Bucket: S3_BUCKET },
//             region: REGION,
//         })
        
//     const [progress, setProgress] = useState(0);
//     const [selectedFile, setSelectedFile] = useState(null);
//     const handleFileInput = (e) => {
//         setSelectedFile(e.target.files[0]);
//     }
//     const uploadFile = (file) => {
//         const params = {
//             ACL: 'public-read',
//             Body: file,
//             Bucket: S3_BUCKET,
//             Key: `${this.state.user.id}/${props.model}`
//         };
//         myBucket.putObject(params)
//             .on('httpUploadProgress', (evt) => {
//                 setProgress(Math.round((evt.loaded / evt.total) * 100))
//             })
//             .send((err) => {
//                 if (err) console.log(err)
//             })
//     }
    
//     return (
//         <div>
//             <div>File Upload Progress is {progress}%</div>
//             <input type="file" onChange={handleFileInput} />
//             <br></br>
//             <Link to="/" className="homelink">
//                 <button className="btn btn-success" onClick={() => uploadFile(selectedFile)}> Submit </button>
//             </Link>
//         </div>
//     )
    
// } 
// }

// export default AwsUpload