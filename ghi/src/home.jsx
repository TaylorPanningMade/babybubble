import React from 'react';

function ProfileColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const item = data;
                return (
                    <div key={item.id} className="card mb-3 shadow card text-white bg-dark" height="2fr">
                        <img src={`https://babybubble.s3.us-west-2.amazonaws.com/babybubble/us-west-2/${item.make}/${item.model}`} alt="" className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{item.make}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {item.model}
                            </h6>
                            <p className="card-text">
                                Owner: {item.owner.name}
                            </p>
                        </div>
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item">Condition: {item.condition}</li>
                            <li className="list-group-item">Gender: {item.gender}</li>
                            <li className="list-group-item">Size: {item.size}</li>
                            <li className="list-group-item">{item.description}</li>
                        </ul>
                    </div>
                );
            })}
        </div>
    );
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profileColumns: [[], [], []]
        };
    }

    async componentDidMount() {
        //const dogsHost = `${process.env.REACT_APP_MONOLITH_API}`
        const itemHost = 'http://localhost:8000'
        const url = itemHost + `/api/list_items/`;
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let item of data.items) {
                    //const dogHost = `${process.env.REACT_APP_MONOLITH_API}`
                    const itemHost = 'http://localhost:8000'
                    const detailUrl = itemHost + `/api/list_items/${item.id}/`;
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const profileColumns = [[], [], []];
                let i = 0;
                for (const itemResponse of responses) {
                    if (itemResponse.ok) {
                        const details = await itemResponse.json();
                        profileColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(itemResponse);
                    }
                }
                this.setState({ profileColumns: profileColumns });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
            <div className="App">
                <div
                    className="container-fluid d-flex align-items-center"
                    style={{
                        height: "100vh",
                        width: '100vw',
                        backgroundImage:
                            "url(https://res.litfad.com/site/img/item/2020/09/10/1714569/1200x1200.jpg)",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "repeat",
                        opacity: 1.0,
                    }}
                >
                
                    <div className="container">
                        <h2>Items in your bubble</h2>
                        <div className="row">
                            {this.state.profileColumns.map((itemList, index) => {
                                return (
                                    <ProfileColumn key={index} list={itemList} />
                                );
                            })}
                        </div>
                    </div>
                    </div>
                    </div>
                    </>
        );
    }
}

export default Home;

