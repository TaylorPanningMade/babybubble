import React from 'react';
import ReactDOM from 'react-dom/client';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import './index.css';
import NavbarDarkExample from './Nav';
import Home from './home';
import Additemform from './Additemform';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Login from './accounts/login';
import Signup from './accounts/signup';
import Landing from './Landing';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>
);

function Router() {
  return (
    <BrowserRouter>
      <NavbarDarkExample />
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/Additem/" element={<Additemform />} />
        <Route path="/Login/" element={<Login />} />
        <Route path="/Signup/" element={<Signup />} />
      </Routes>
    </BrowserRouter>
  );
};


